#include <stdbool.h>
#include <stdint.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#include <math.h>

#define MPU9250_ADDRESS 0x68
#define AXP209_ADDRESS 0x34

#define    ACC_FULL_SCALE_2_G        0x00  
#define    ACC_FULL_SCALE_4_G        0x08
#define    ACC_FULL_SCALE_8_G        0x10
#define    ACC_FULL_SCALE_16_G       0x18
#define    GYRO_FULL_SCALE_2000_DPS   0x18

#define G_GAIN 0.07
#define M_PI 3.14159265358979323846
#define RAD_TO_DEG 57.29578

#define MSP_PACKET_LENGTH 22
#define KEEPALIVE 50 //in milliseconds, when there is more than KEEPALIVE ms elapsed we resend the packet even if it is the same
#define RC_MAX_RATE 20000 //max 1 packet every MAX_RATE microseconds


int readReg(int fd, int reg);
void writeReg(int fd, int reg, uint8_t val);
int read_acc_gyr_values(int i2c_handle, int16_t *ax, int16_t *ay, int16_t *az, int16_t *gx, int16_t *gy, int16_t *gz);