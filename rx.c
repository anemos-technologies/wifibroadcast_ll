// (c)2017 Anemos Technologies (pcrochat@anemos-technologies.com)

/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program; if not, write to the Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
 
#include "fec.h"

#include "lib.h"
#include "radiotap.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>

#define MAX_PACKET_LENGTH 1024
#define MAX_USER_PACKET_LENGTH 1450
#define MAX_DATA_OR_FEC_PACKETS_PER_BLOCK 64

#define DEBUG 0
#define VERBOSE 0
#define HDMI 1

#define debug_print(fmt, ...) \
            do { if (DEBUG) fprintf(stderr, fmt, __VA_ARGS__); } while (0)

#define xstr(s) str(s)
#define str(s) #s            	
            	
#define ARP_CACHE       "/proc/net/arp"
#define ARP_STRING_LEN  1023
#define ARP_BUFFER_LEN  (ARP_STRING_LEN + 1)


#define delay_ms(a) usleep(a*1000)

/* Format for fscanf() to read the 1st, 4th, and 6th space-delimited fields */
#define ARP_LINE_FORMAT "%" xstr(ARP_STRING_LEN) "s %*s %*s " \
                        "%" xstr(ARP_STRING_LEN) "s %*s " \
                        "%" xstr(ARP_STRING_LEN) "s"

// this is where we store a summary of the
// information from the radiotap header

typedef struct  {
	int m_nChannel;
	int m_nChannelFlags;
	int m_nRate;
	int m_nAntenna;
	int m_nRadiotapFlags;
} __attribute__((packed)) PENUMBRA_RADIOTAP_DATA;



int flagHelp = 0;
int param_window_length = 50;

int win_bot = 0;
int win_top = 0;

int selectable_fd_video;
int selectable_fd_telemetry;
//int selectable_fd_rc_commands;

int8_t current_signal_dbm;

unsigned int n80211HeaderLength_video;
unsigned int n80211HeaderLength_telemetry;
//unsigned int n80211HeaderLength_rc_commands;

pcap_t *ppcap_video;
pcap_t *ppcap_telemetry;
//pcap_t *ppcap_rc_commands;

int clientSocket, portNum, nBytes;
char buffer[1024];
struct hostent *hp; /* host information */
struct sockaddr_in serverAddr;
socklen_t addr_size;
unsigned int block_sent=0;

pthread_t tid[2];

uint32_t max_packet_counter_video=0;
uint32_t max_packet_counter_telemetry=0;

uint32_t last_sequence_received=0;
uint32_t last_nalu_index=0;
uint32_t nalu_length=0;
uint32_t nalu_current_index=0;

unsigned int data_nbr=0;
unsigned int fec_nbr=0;

uint8_t *data_blocks[MAX_DATA_OR_FEC_PACKETS_PER_BLOCK];
uint8_t *good_fec[MAX_DATA_OR_FEC_PACKETS_PER_BLOCK];

uint8_t *corrupted_data_blocks[MAX_DATA_OR_FEC_PACKETS_PER_BLOCK];
uint8_t *corrupted_fec_blocks[MAX_DATA_OR_FEC_PACKETS_PER_BLOCK];


uint8_t data_blob[MAX_DATA_OR_FEC_PACKETS_PER_BLOCK*MAX_PACKET_LENGTH];
uint8_t fec_blob[MAX_DATA_OR_FEC_PACKETS_PER_BLOCK*MAX_PACKET_LENGTH];


unsigned int erased_blocks[MAX_DATA_OR_FEC_PACKETS_PER_BLOCK];
unsigned int fec_block_nos[MAX_DATA_OR_FEC_PACKETS_PER_BLOCK];
unsigned int good_data[MAX_DATA_OR_FEC_PACKETS_PER_BLOCK];

//unsigned int corrupted_data[MAX_DATA_OR_FEC_PACKETS_PER_BLOCK];
//unsigned int corrupted_fec[MAX_DATA_OR_FEC_PACKETS_PER_BLOCK];

//unsigned int nr_corrupted_data_blocks=0;
//unsigned int nr_corrupted_fec_blocks=0;
unsigned int nr_good_data_blocks=0;
unsigned int nr_good_fec_blocks=0;
unsigned int nr_erased_blocks=0;

uint8_t message_to_phone[MAX_PACKET_LENGTH];

int package_sent=0;
int last_block_sent=0;

uint32_t nalu_last_index=0;

struct timeval rc_last_sent;
struct timeval now;
struct timeval timer_result;
struct timeval last_measured;
float DT;

int fd_i2c1, fd_i2c0;
int ret;
int x;

static const u8 u8aRadiotapHeader[] = {
	0x00, 0x00, // <-- radiotap version
	0x0c, 0x00, // <- radiotap header lengt
	0x04, 0x80, 0x00, 0x00, // <-- bitmap
	0x22, 
	0x0, 
	0x18, 0x00 
};


//the last byte of the mac address is recycled as a port number
#define SRC_MAC_LASTBYTE 15
#define DST_MAC_LASTBYTE 21

static u8 u8aIeeeHeader[] = {
	0x08, 0x01, 0x00, 0x00,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0x13, 0x22, 0x33, 0x44, 0x55, 0x66,
	0x13, 0x22, 0x33, 0x44, 0x55, 0x66,
	0x10, 0x86,
};

void usage(void)
{
	printf(
	    "(c)2017 anemos technologies (pcrochat@anemos-technologies.com). Based on wifibroadcast by befinitv.  Licensed under GPL2\n"
	    "\n"
	    "Usage: rx [options] <interfaces>\n\n"
	    "Example:\n"
	    "  iwconfig wlan0 down\n"
	    "  iw dev wlan0 set monitor otherbss fcsfail\n"
	    "  ifconfig wlan0 up\n"
			"  iwconfig wlan0 channel 13\n"
	    "  rx wlan0        Receive raw packets on wlan0 and redirect to detected phone/tablet ip address\n"
	    "\n");
			
	exit(1);
}

void set_port_no(uint8_t *pu, uint8_t port) {
	//dirty hack: the last byte of the mac address is the port number. this makes it easy to filter out specific ports via wireshark
	pu[sizeof(u8aRadiotapHeader) + SRC_MAC_LASTBYTE] = port;
	pu[sizeof(u8aRadiotapHeader) + DST_MAC_LASTBYTE] = port;
}

int packet_header_init(uint8_t *packet_header) {
			u8 *pu8 = packet_header;
			memcpy(packet_header, u8aRadiotapHeader, sizeof(u8aRadiotapHeader));
			pu8 += sizeof(u8aRadiotapHeader);
			memcpy(pu8, u8aIeeeHeader, sizeof (u8aIeeeHeader));
			pu8 += sizeof (u8aIeeeHeader);
					
			//determine the length of the header
			return pu8 - packet_header;
}


void open_and_configure_interface(pcap_t *ppcap, int port, unsigned int *n80211HeaderLength, int *selectable_fd) {
	struct bpf_program bpfprogram;
	char szProgram[512];

	int nLinkEncap = pcap_datalink(ppcap);

	switch (nLinkEncap) {

		case DLT_PRISM_HEADER:
			fprintf(stderr, "DLT_PRISM_HEADER Encap\n");
			*n80211HeaderLength = 0x20; // ieee80211 comes after this
			sprintf(szProgram, "radio[0x4a:4]==0x13223344 && radio[0x4e:2] == 0x55%.2x", port);
			break;

		case DLT_IEEE802_11_RADIO:
			fprintf(stderr, "DLT_IEEE802_11_RADIO Encap\n");
			*n80211HeaderLength = 0x18; // ieee80211 comes after this
			sprintf(szProgram, "ether[0x0a:4]==0x13223344 && ether[0x0e:2] == 0x55%.2x", port);
			break;

		default:
			fprintf(stderr, "!!! unknown encapsulation on %d !\n", port);
			exit(1);
	}

	if (pcap_compile(ppcap, &bpfprogram, szProgram, 1, 0) == -1) {
		fprintf(stderr, "%s\n", szProgram);
		fprintf(stderr, "%s\n", pcap_geterr(ppcap));
		exit(1);
	} else {
		if (pcap_setfilter(ppcap, &bpfprogram) == -1) {
			fprintf(stderr, "%s\n", szProgram);
			fprintf(stderr, "%s\n", pcap_geterr(ppcap));
		} else {
		}
		pcap_freecode(&bpfprogram);
	}

	*selectable_fd = pcap_get_selectable_fd(ppcap);
}

void send_phone(int destination_port, uint8_t *data, size_t data_len)
{
	serverAddr.sin_port = htons(destination_port);
	
	//if(checksum_correct==1 && send_packet==1)
	//{
	/*Send message to phone*/
	/* send a message to the server */ 
	
	if(HDMI==1 && DEBUG==0)
	{
		if(destination_port==5000)
			write(STDOUT_FILENO, data, data_len);
	}
	
	if(DEBUG==0 && HDMI==0)
	{
		if (sendto(clientSocket, data, data_len, 0, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0) 
		{ 
			printf("sendto failed\n"); 
			//return 0; 
		}
		
		usleep(20);
		
		//sendto(clientSocket,data,data_len,0,(struct sockaddr *)&serverAddr,addr_size);
		
		if(destination_port==5001)
		{
			serverAddr.sin_port = htons(5003);
			
			// send rssi 
			if (sendto(clientSocket, &current_signal_dbm, 1, 0, (struct sockaddr *)&serverAddr,sizeof(serverAddr)) < 0) 
			{ 
				printf("sendto rssi failed\n"); 
			}
			
			//max_packet_counter_telemetry=block_num;
		}
	/*	else
		{
			max_packet_counter_video=block_num;
		}*/
	}
}

void send_packet()
{
	unsigned int i,j,found;
	
	if(package_sent==0 && data_nbr>0)
	{
		//printf("send phone block\n");
		
		if(data_nbr-nr_good_data_blocks>nr_good_fec_blocks)
		{
			fprintf(stderr, "could not reconstruct data frame\n");
			
			//if(block_sent>1)
				//exit(0);
		}
		
		if(data_nbr-nr_good_data_blocks<=nr_good_fec_blocks && data_nbr>nr_good_data_blocks)
		{
			for(i=0;i<data_nbr;i++)
			{
				found=0;
				
				for(j=0;j<nr_good_data_blocks;j++)
				{
					if(good_data[j]==i)
					{
						found=1;
						break;
					}
				}
				
				if(found==0)
				{
					erased_blocks[nr_erased_blocks]=i;
					nr_erased_blocks++;
				}
			}
			
			fprintf(stderr, "nr_erased_blocks : %d %d %d %d\n", nr_erased_blocks, nr_good_fec_blocks, data_nbr, fec_nbr);
			fec_decode(MAX_PACKET_LENGTH, data_blocks, data_nbr, good_fec, fec_block_nos, erased_blocks,nr_erased_blocks);
		}
		
		for(i=0;i<data_nbr;i++)
		{
			//write(STDOUT_FILENO, data_blob+i*MAX_PACKET_LENGTH, MAX_PACKET_LENGTH);
			
			if(DEBUG==1)
				printf("send to video %d\n", MAX_PACKET_LENGTH);
			
			send_phone(5000, data_blob+i*MAX_PACKET_LENGTH, MAX_PACKET_LENGTH);
		}
		
		package_sent=1;
		block_sent++;
	}
}

int process_payload(uint8_t *data, size_t data_len, int destination_port, int checksum_correct)
{
    wifi_packet_header_t *wph;
    
    uint8_t data_index;
    wph = (wifi_packet_header_t*)data;
    data += sizeof(wifi_packet_header_t);
    data_len -= sizeof(wifi_packet_header_t);
    
    if(DEBUG==1)
    	printf("receive %d\n", wph->sequence_number);
    
    uint32_t nalu_index=wph->nalu_index;
    uint8_t fec_index=wph->fec_index;
    
    if(destination_port>5000)
    {
    	if(checksum_correct==1)
    	{
    		send_phone(destination_port, data, data_len);
    	}
    	
    	return 0;
    }
    
    uint32_t nalu_compare = nalu_last_index > 50 ? nalu_last_index-50 : 0;
    
    //detect tx restart
    if((nalu_index>nalu_last_index || nalu_index < nalu_compare) && checksum_correct==1)
    {
    	if(package_sent==0 && data_nbr>0)
    	{
    		fprintf(stderr, "new nalu send\n");
    		send_packet();
    	}
    	
    	//determine new nalu frame data and fec size
    	nalu_length=wph->nalu_length;
    	
    	data_nbr=(int)(nalu_length/MAX_PACKET_LENGTH);
    	fec_nbr=data_nbr/2+data_nbr%2;
    	
    	if(DEBUG==1 || VERBOSE==1)
    	{
    		fprintf(stderr, "\n begin new nalu %d %d\n",  data_nbr, fec_nbr);
    	}
    	
    	//nr_corrupted_data_blocks=0;
    	//nr_corrupted_fec_blocks=0;
    	nr_good_data_blocks=0;
    	nr_good_fec_blocks=0;
    	nr_erased_blocks=0;
    	
    	package_sent=0;
    	last_block_sent=0;
    	
    	nalu_last_index=nalu_index;
    }
    
    if(checksum_correct==1)
	{	
		if(data_len==MAX_PACKET_LENGTH)
		{
			if(fec_index%2==0 || fec_index>=(fec_nbr<<1))
			{
				data_index = fec_index>=(fec_nbr<<1) ? fec_nbr+fec_index-(fec_nbr<<1) : (fec_index>>1);
				
				//write(STDOUT_FILENO, data, data_len);
				memcpy(data_blob+data_index*MAX_PACKET_LENGTH,data,data_len);
				
				good_data[nr_good_data_blocks]=data_index;
				nr_good_data_blocks++;
			}
			else
			{
				int fec_offset=(fec_index>>1)*MAX_PACKET_LENGTH;
				
				memcpy(fec_blob+fec_offset,data,data_len);
				
				good_fec[nr_good_fec_blocks]=fec_blob+fec_offset;
				fec_block_nos[nr_good_fec_blocks]=(fec_index>>1);
				
				nr_good_fec_blocks++;
			}
		}
		else
		{
			send_packet();
			
			if(last_block_sent==0)
			{
				//write(STDOUT_FILENO, data, data_len);
				//printf("send phone end\n");
				if(DEBUG==1)
					printf("send to video %d\n", data_len);
				
				send_phone(5000,data, data_len);
				last_block_sent=1;
			}
		}
    }
    
    
    if(DEBUG==1 || VERBOSE==1)
    {
    	fprintf(stderr, "%u %d\n", wph->sequence_number, data_len);
    	fprintf(stderr, "%d %d %d | %d %d | %d | %d | %d %d %d %d\n", fec_index, data_index, (fec_index>>1), nr_good_data_blocks, nr_good_fec_blocks, good_data[nr_good_data_blocks], erased_blocks[nr_erased_blocks], nr_erased_blocks, data_len, current_signal_dbm, checksum_correct);
    }
    
    if(wph->sequence_number>(last_sequence_received+1))
    {
    	fprintf(stderr, "sequence %u lost %u \n", last_sequence_received, wph->sequence_number);
    }
    
    if(checksum_correct==0)
    {
    	fprintf(stderr, "sequence %u checksum incorrect\n", wph->sequence_number);
    }
    
    last_sequence_received=wph->sequence_number;
    
    return 0;
}


void process_packet(pcap_t *ppcap, unsigned int n80211HeaderLength, int destination_port) {
		struct pcap_pkthdr * ppcapPacketHeader = NULL;
		struct ieee80211_radiotap_iterator rti;
		PENUMBRA_RADIOTAP_DATA prd;
		u8 payloadBuffer[MAX_PACKET_LENGTH];
		u8 *pu8Payload = payloadBuffer;
		int bytes;
		int n;
		int retval;
		unsigned int u16HeaderLen;
		
		// receive
		retval = pcap_next_ex(ppcap, &ppcapPacketHeader,
		    (const u_char**)&pu8Payload);

		if (retval < 0) {
			fprintf(stderr, "Socket broken\n");
			fprintf(stderr, "%s\n", pcap_geterr(ppcap));
			exit(1);
		}

		//if(retval == 0)
		//	fprintf(stderr, "retval = 0\n");

		if (retval != 1)
			return;


		u16HeaderLen = (pu8Payload[2] + (pu8Payload[3] << 8));

		if (ppcapPacketHeader->len <
		    (u16HeaderLen + n80211HeaderLength))
			return;

		bytes = ppcapPacketHeader->len -
			(u16HeaderLen + n80211HeaderLength);
		if (bytes < 0)
			return;

		if (ieee80211_radiotap_iterator_init(&rti,
		    (struct ieee80211_radiotap_header *)pu8Payload,
		    ppcapPacketHeader->len) < 0)
			return;

		while ((n = ieee80211_radiotap_iterator_next(&rti)) == 0) {

			switch (rti.this_arg_index) {
			case IEEE80211_RADIOTAP_RATE:
				prd.m_nRate = (*rti.this_arg);
				break;

			case IEEE80211_RADIOTAP_CHANNEL:
				prd.m_nChannel =
				    le16_to_cpu(*((u16 *)rti.this_arg));
				prd.m_nChannelFlags =
				    le16_to_cpu(*((u16 *)(rti.this_arg + 2)));
				break;

			case IEEE80211_RADIOTAP_ANTENNA:
				prd.m_nAntenna = (*rti.this_arg) + 1;
				break;

			case IEEE80211_RADIOTAP_FLAGS:
				prd.m_nRadiotapFlags = *rti.this_arg;
				break;
				
			case IEEE80211_RADIOTAP_DBM_ANTSIGNAL:
				current_signal_dbm = (int8_t)(*rti.this_arg);
				break;
			}
		}
		
		int checksum_correct = (prd.m_nRadiotapFlags & 0x40) == 0;
		
		pu8Payload += u16HeaderLen + n80211HeaderLength;

		if (prd.m_nRadiotapFlags & IEEE80211_RADIOTAP_F_FCS)
			bytes -= 4;


        process_payload(pu8Payload, bytes, destination_port, checksum_correct);
}

void* process_video(void *arg)
{
	while(1)
	{
		fd_set readset;
		struct timeval to;
		
		to.tv_sec = 0;
		to.tv_usec = 1e5;
		
		FD_ZERO(&readset);
		FD_SET(selectable_fd_video, &readset);
		FD_SET(selectable_fd_telemetry, &readset);
		
		int n = select(30, &readset, NULL, NULL, &to);
		
		if(n != 0)
		{
			//printf("buffer ready \n");
			//receive video
			if(FD_ISSET(selectable_fd_video, &readset)) {
				process_packet(ppcap_video, n80211HeaderLength_video, 5000);
			}
			
			//receive telemetry
			if(FD_ISSET(selectable_fd_telemetry, &readset)) {
				process_packet(ppcap_telemetry, n80211HeaderLength_telemetry, 5001);
			}
		}
	}
		
	return NULL;
}


int main(int argc, char *argv[])
{
	FILE *arpCache; 
	char *ipAddr, hwAddr[ARP_BUFFER_LEN], device[ARP_BUFFER_LEN];
	ipAddr=(char *)malloc(ARP_BUFFER_LEN);
	int i;
	
	fec_init();
	
	while (1) {
		int nOptionIndex;
		static const struct option optiona[] = {
			{ "help", no_argument, &flagHelp, 1 },
			{ 0, 0, 0, 0 }
		};
		int c = getopt_long(argc, argv, "hw:",
			optiona, &nOptionIndex);

		if (c == -1)
			break;
		switch (c) {
		case 0: // long option
			break;

		case 'h': // help
			usage();
		case 'w':
            param_window_length = atoi(optarg);
			break;

		default:
			fprintf(stderr, "unknown switch %c\n", c);
			usage();
			break;
		}
	}

	//initialize data_blocks
	for(i=0;i<MAX_DATA_OR_FEC_PACKETS_PER_BLOCK;i++)
	{
		data_blocks[i]=data_blob+i*MAX_PACKET_LENGTH;
	}
	
	if(DEBUG==0 && HDMI==0)
	{
		int ip_address_found=0;
		
		while(ip_address_found==0)
		{
			arpCache=fopen(ARP_CACHE, "r");
			
			if (!arpCache)
			{
				perror("Arp Cache: Failed to open file \"" ARP_CACHE "\"");
				return 1;
			}
			
			//Ignore the first line, which contains the header
			char header[ARP_BUFFER_LEN];
			if (!fgets(header, sizeof(header), arpCache))
			{
				return 1;
			}
			
			while (3 == fscanf(arpCache, ARP_LINE_FORMAT, ipAddr, hwAddr, device))
			{
				if(strcmp(device,"usb1")==0 || strcmp(device,"usb0")==0)
				{
					printf("ip adress found is %s %s\n",ipAddr, device);
					ip_address_found=1;
					break;
				}
			}
			
			fclose(arpCache);
			
			if(ip_address_found==0)
				usleep(100000);
			
			printf("looking for an ip address...\n");
		}
		
		//Create UDP socket
		clientSocket = socket(PF_INET, SOCK_DGRAM, 0);
		
		if (clientSocket < 0) 
			fprintf(stderr, "ERROR opening socket");
		
		// fill in the server's address and data  
		memset((char*)&serverAddr, 0, sizeof(serverAddr)); 
		serverAddr.sin_family = AF_INET;  
		serverAddr.sin_port = htons(5000); 
		
		// look up the address of the server given its name  
		hp = gethostbyname(ipAddr); 
		if (!hp) 
		{ 
			fprintf(stderr, "could not obtain address of %s\n", ipAddr); 
			return 0; 
		} // put the host's address into the server address structure  
		
		memcpy((void *)&serverAddr.sin_addr, hp->h_addr_list[0], hp->h_length); 
	}
	
	if (optind >= argc)
		usage();

	int u = optind;
	
	char szErrbuf[PCAP_ERRBUF_SIZE];
	
	// open the interface video in pcap
	szErrbuf[0] = '\0';
	ppcap_video = pcap_open_live(argv[u], 2048, 1, -1, szErrbuf);
	if (ppcap_video == NULL) {
		fprintf(stderr, "Unable to open interface %s in pcap: %s\n",
		    argv[u], szErrbuf);
		exit(1);
	}
	

	if(pcap_setnonblock(ppcap_video, 1, szErrbuf) < 0) {
		fprintf(stderr, "Error setting %s to nonblocking mode: %s\n", argv[u], szErrbuf);
	}
	
	open_and_configure_interface(ppcap_video, 0, &n80211HeaderLength_video, &selectable_fd_video);
	
	// open the interface telemetry in pcap
	ppcap_telemetry = pcap_open_live(argv[u], 2048, 1, -1, szErrbuf);
	if (ppcap_telemetry == NULL) {
		fprintf(stderr, "Unable to open interface %s in pcap: %s\n",
		    argv[u], szErrbuf);
		exit(1);
	}
	

	if(pcap_setnonblock(ppcap_telemetry, 1, szErrbuf) < 0) {
		fprintf(stderr, "Error setting %s to nonblocking mode: %s\n", argv[u], szErrbuf);
	}
	
	//telemetry configure interface
	open_and_configure_interface(ppcap_telemetry, 1, &n80211HeaderLength_telemetry, &selectable_fd_telemetry);
	
	//ms_open();
	
	int err;
	
	err = pthread_create(&(tid[0]), NULL, &process_video, NULL);
	
	if (err != 0)
            fprintf(stderr, "\ncan't create thread :[%s]", strerror(err));
	
    /*err = pthread_create(&(tid[1]), NULL, &process_stick, NULL);
	
	if (err != 0)
            printf("\ncan't create thread :[%s]", strerror(err));
	*/
	
	for(;;) { 
		//fprintf(stderr, "begin loop:\n");
		usleep(10000);
	}

	return (0);
}
