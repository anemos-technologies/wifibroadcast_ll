
LDFLAGS=-lrt -lpcap -lm -lpthread -O2
CPPFLAGS=-std=gnu99 -O2

all: rx txhook.so

txhook.so: txhook.c fec.c
	gcc $(CPPFLAGS) $(LDFLAGS) -fPIC -shared -o $@ $^

%.o: %.c
	gcc -c -o $@ $< $(CPPFLAGS)


rx: rx.o radiotap.o fec.o
	gcc -o $@ $^ $(LDFLAGS)

	
clean:
	rm -f rx *~ *.o *.so

