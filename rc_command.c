// (c)2017 Philippe Crochat - Anemos Technologies - www.anemos-technologies.com
/*   pcrochat@anemos-technologies.com
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program; if not, write to the Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/* implements multiwii protocol on chip with i2c calls (only works with anemos rc transmitter */ 

#include "rc_command.h"

int16_t ax,ay,az;
int16_t gx,gy,gz;

int readReg(int fd, int reg)
{
  uint8_t regbuf[1];
	regbuf[0] = reg  & 0xFF;
	int res = write(fd, regbuf, sizeof(regbuf));
	if (res < 0) {
	  fprintf(stderr,"cannot read register\n");
	  exit(1);
	}

  uint8_t valbuf[1];
  res = read(fd, valbuf, sizeof(valbuf));
  if (res < 0) {
	  fprintf(stderr, "cannot read register\n");
	  exit(1);
	}

  return valbuf[0];
}

void writeReg(int fd, int reg, uint8_t val)
{
  uint8_t cmdbuf[2];
	cmdbuf[0] = reg  & 0xFF;
	cmdbuf[1] = val;
	int res = write(fd, cmdbuf, sizeof(cmdbuf));
	if (res < 0) {
	  fprintf(stderr, "cannot write register\n");
	  exit(1);
	}
}

int read_acc_gyr_values(int i2c_handle, int16_t *ax, int16_t *ay, int16_t *az, int16_t *gx, int16_t *gy, int16_t *gz)
{
	int i;
	uint8_t Buf[14];
	
	for (i=0;i<sizeof(Buf);i++)
	{
		Buf[i]=readReg(i2c_handle, 0x3B+i);
	}
	
	// Create 16 bits values from 8 bits data
	
	// Accelerometer
	*ax=(Buf[0]<<8 | Buf[1]);
	*ay=(Buf[2]<<8 | Buf[3]);
	*az=(Buf[4]<<8 | Buf[5]);
	
	// Gyroscope
	*gx=(Buf[8]<<8 | Buf[9]);// * G_GAIN;
	*gy=(Buf[10]<<8 | Buf[11]);// * G_GAIN;
	*gz=(Buf[12]<<8 | Buf[13]);// * G_GAIN;
	
	return true;
}



int calibration(int i2c_handle, int16_t *offset_array)
{
	int i,j;
	int16_t acc_gyro_values[6];
	long int total_acc_gyro_values[6];
	
	for(j=0;j<6;j++)
		total_acc_gyro_values[j]=0;
	
	for(i=0;i<100;i++)
	{
		read_acc_gyr_values(i2c_handle, acc_gyro_values, acc_gyro_values+1, acc_gyro_values+2, acc_gyro_values+3, acc_gyro_values+4, acc_gyro_values+5);
		
		for(j=0;j<6;j++)
		{
			total_acc_gyro_values[j]+=acc_gyro_values[j];
		}
	}
	
	for(j=0;j<6;j++)
	{
		offset_array[j]=total_acc_gyro_values[j]/100;
	}
	
	offset_array[2]-=1.0; //z axis accelerometer
	
	return true;
}

int main(int argc, char *argv[])
{
	long int throttle_position, throttle_saved_value, throttle2;
	int yaw_position;
	
	uint8_t packet_data[MSP_PACKET_LENGTH];
	uint8_t old_packet_data[MSP_PACKET_LENGTH];
	uint8_t crc;
	int channel_values[8];
	int16_t offset_array[6];
	
	struct timeval rc_last_sent;
    struct timeval now;
    struct timeval timer_result;
	struct timeval last_measured;
	float DT;
	
    int fd_i2c1, fd_i2c0;
	int ret;
    int x;
    
    float AccXangle, AccYangle;
    float gyroXangle=0;
    float gyroYangle=0;
    float gyroZangle=0;
    
    float CFangleX=0;
    float CFangleY=0;
    
	//intialize i2c
	fd_i2c1 = open("/dev/i2c-1", O_RDWR);
	if (fd_i2c1 < 0) {
		perror("cannot open I2C bus");
		return 1;
	}
	
	//intialize i2c
	fd_i2c0 = open("/dev/i2c-0", O_RDWR);
	if (fd_i2c0 < 0) {
		perror("cannot open I2C bus");
		return 1;
	}
	
	ret = ioctl(fd_i2c1, I2C_SLAVE_FORCE, MPU9250_ADDRESS);
	if (ret < 0) {
		fprintf(stderr, "cannot talk to MPU9250\n");
		return 1;
	}
	
	// Configure accelerometers range
	writeReg(fd_i2c1, 28, ACC_FULL_SCALE_16_G);
	
	// Configure gyroscope range
	writeReg(fd_i2c1, 27, GYRO_FULL_SCALE_2000_DPS);
	
	ret = ioctl(fd_i2c0, I2C_SLAVE_FORCE, AXP209_ADDRESS);
	if (ret < 0) {
		fprintf(stderr, "cannot talk to AXP209\n");
		return 1;
	}
	
	writeReg(fd_i2c0, 0x83, 0x80); //disable ADC input on GPIO0 and GPIO1
	writeReg(fd_i2c0, 0x90, 0x04); //set GPIO0 to 12 bit ADC input
	writeReg(fd_i2c0, 0x92, 0x04); //set GPIO1 to 12 bit ADC input
	writeReg(fd_i2c0, 0x85, 0x00); //set ADC input range to 0-2v
	writeReg(fd_i2c0, 0x83, 0x8C); // enable ADC input on GPIO0 and GPIO1
	
	fprintf(stderr, "calibrating IMU...\n");
	calibration(fd_i2c1, offset_array);
	fprintf(stderr, "calibration complete %d %d %d %d %d %d\n", offset_array[0], offset_array[1], offset_array[2], offset_array[3], offset_array[4], offset_array[5]);
	
	
	read_acc_gyr_values(fd_i2c1, &ax, &ay, &az, &gx, &gy, &gz);
	gettimeofday(&last_measured, NULL);
	
	
	for(;;)
	{
		read_acc_gyr_values(fd_i2c1, &ax, &ay, &az, &gx, &gy, &gz);
		
		gx=gx-offset_array[3];
		gy=gy-offset_array[4];
		gz=gz-offset_array[5];
		
		gettimeofday(&now, NULL);
		timersub(&now, &last_measured, &timer_result);
		
		DT=timer_result.tv_usec/1000000.0;
		
		AccXangle = -(float) (atan2(ay,az))*RAD_TO_DEG;
		AccYangle = (float) (atan2(az,ax)-M_PI/2)*RAD_TO_DEG;
		
		gyroXangle+=((float)gx)*G_GAIN*DT;
		gyroYangle+=((float)gy)*G_GAIN*DT;
		gyroZangle+=((float)gz)*G_GAIN*DT;
		
		CFangleX=0.98*(CFangleX+((float)gx)*G_GAIN*DT) +(1 - 0.98) * AccXangle;
		CFangleY=0.98*(CFangleY+((float)gy)*G_GAIN*DT) +(1 - 0.98) * AccYangle;
		
		//fprintf(stderr, "\n");
		//fprintf(stderr, "%d	%d	%d 	%d	%d	%d\n", ax, ay, az, gx, gy ,gz);
		//fprintf(stderr, "AccelX %f	AccelY %f	gyroX %f	gyroY %f	angleX %f	angleY %f\n", AccXangle, AccYangle, gyroXangle, gyroYangle, CFangleX, CFangleY);
		
		uint8_t p=readReg(fd_i2c0, 0x85);
		uint8_t rhigh;
		
		x=0;
		while((rhigh=readReg(fd_i2c0, 0x64))<2)
		{
			x++;
			if(x>1)
				break;
		}
		
		uint8_t rlow=readReg(fd_i2c0, 0x65);
		unsigned long int r=(rhigh<<8)+rlow;
		
		throttle_saved_value=rhigh;
		throttle2=rlow;
		throttle_position=(((r *10000)/4096)*2)+p*7000;
		
		x=0;
		while((rhigh=readReg(fd_i2c0, 0x66))<2)
		{
			x++;
			if(x>1)
				break;
		}
		
		rlow=readReg(fd_i2c0, 0x67);
		r=(rhigh<<8)+rlow;
		yaw_position=(((r *10000)/4096)*2)+p*7000;
		
		
		packet_data[0] = '$';
		packet_data[1] = 'M';
		packet_data[2] = '<';
		packet_data[3] = 16; //8 channels 2 bytes
		packet_data[4] = 200; // MSP_SET_RAW_RC
		
		
		channel_values[0]=1500+(int)(AccXangle/90*400);
		channel_values[1]=1500+(int)(AccYangle/90*400);
		channel_values[2]=1500-(throttle_position-80000)/200;
		channel_values[3]=1500+(yaw_position-80000)/200;
		
		for(x=0;x<4;x++)
		{
			channel_values[x+4]=1000;
		}
		
		for(x=0;x<8;x++)
		{
			packet_data[5+2*x]=(uint8_t)(channel_values[x]&0xFF);
			packet_data[6+2*x]=(uint8_t)(channel_values[x]>>8);
		}
		
		//build crc 
		crc=0;
		for(x=3;x<MSP_PACKET_LENGTH-1;x++)
		{
			crc^=packet_data[x];
		}
		
		packet_data[MSP_PACKET_LENGTH-1]=crc;
		
		
		int transmit_packet=0;
		
		for(x=0;x<MSP_PACKET_LENGTH;x++)
		{
			if(packet_data[x]!=old_packet_data[x])
			{
				//fprintf(stderr, "%d:%d:%d\n", x, packet_data[x], old_packet_data[x]);
				transmit_packet=1;
				break;
			}
		}
		
		
		gettimeofday(&now, NULL);
		timersub(&now, &rc_last_sent, &timer_result);
		
		//printf("%d\n",(int) timer_result.tv_usec/1000);
		if(((transmit_packet==1)) || ((timer_result.tv_usec/1000)>KEEPALIVE))
		{
			gettimeofday(&now, NULL);
			timersub(&now, &rc_last_sent, &timer_result);
			
			//fprintf(stderr, "timer : %d, %d\n", (int)timer_result.tv_usec, JOY_MAX_RATE);
			if(timer_result.tv_usec>RC_MAX_RATE)
			{
				//for(x=0;x<8;x++)
				//{
					//fprintf(stderr, "%d:%d, ", x, channel_values[x]);
				//}
				
				//fprintf(stderr, "\n");
				
				//fprintf(stderr, "%ld	%ld	%d\n", throttle_saved_value, throttle2, channel_values[2]);
				
				//ensure the receiver receives the latest packet by sending it twice
				for(x=0;x<2;x++)
				{
					write(STDOUT_FILENO, packet_data, MSP_PACKET_LENGTH); //MSP pack
				}
				
				gettimeofday(&rc_last_sent, NULL);
			}
		}
	}
					
	return 0;
}