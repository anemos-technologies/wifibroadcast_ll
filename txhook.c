// (c)2017 Philippe Crochat - www.anemos-technologies.com based on low latency hook from wifibroadcast/befinitiv

/*
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program; if not, write to the Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


/* This library is a hook that overwrites the fwrite function and transmits
everything received via wifibroadcast.

Usage example:

LD_PRELOAD=./txhook.so raspivid -cd H264 -n -ih -pf baseline -h 810 -w 960 -b 3000000 -fps 49 -g 1 -t 0 -o -

*/


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>

#include <termios.h>		//Used for UART

#include <time.h>

#include "fec.h"

#include "lib.h"
#include "wifibroadcast.h"
#include "radiotap.h"


#define MAX_PACKET_LENGTH 1024
#define MAX_TELEMETRY_PACKET_SIZE 1024
#define MAX_PACKET_RECEIVE 256
#define MSP_PACKET_LENGTH 22

#define INTERFACE_NAME "wlan0"
#define SERIAL_DEVICE "/dev/ttyAMA0"
#define BAUD_RATE B19200
#define KEEPALIVE 20

int init_done = 0;
uint8_t packet_transmit_buffer_video[MAX_PACKET_LENGTH];
uint8_t packet_transmit_buffer_telemetry[MAX_TELEMETRY_PACKET_SIZE];
uint8_t telemetry_packet[MAX_TELEMETRY_PACKET_SIZE];
uint8_t packet_buffer_receive[MAX_PACKET_RECEIVE];
int discard_packet=0;
u8 payloadBuffer_save[MSP_PACKET_LENGTH];
int rc_commands_stored=0;
struct timeval now;
struct timeval timer_result;
struct timeval rc_last_sent;

int selectable_fd;

int telemetry_packet_index=2;
int telemetry_header_index=0;

char szErrbuf[PCAP_ERRBUF_SIZE];
pcap_t *ppcap = NULL;
pcap_t *ppcap_rc_commands = NULL;

char fBrokenSocket = 0;

uint32_t pcnt_video = 0;
uint32_t pcnt_telemetry = 0;
uint32_t pcnt_frame_count = 0;

size_t packet_header_length_video = 0;
size_t packet_header_length_telemetry = 0;

time_t start_time;
int uart0_filestream = -1;
int n80211HeaderLength;
int8_t current_signal_dbm;
pthread_t tid;
pthread_mutex_t lock;

int selectable_fd;
int fwrite_lock=0;

#define DEBUG 0

static const u8 u8aRadiotapHeader[] = {
	0x00, 0x00, // <-- radiotap version
	0x0c, 0x00, // <- radiotap header lengt
	0x04, 0x80, 0x00, 0x00, // <-- bitmap
	0x22, 
	0x0, 
	0x18, 0x00 
};


//the last byte of the mac address is recycled as a port number
#define SRC_MAC_LASTBYTE 15
#define DST_MAC_LASTBYTE 21

static u8 u8aIeeeHeader[] = {
	0x08, 0x01, 0x00, 0x00,
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
	0x13, 0x22, 0x33, 0x44, 0x55, 0x66,
	0x13, 0x22, 0x33, 0x44, 0x55, 0x66,
	0x10, 0x86,
};

// this is where we store a summary of the
// information from the radiotap header

typedef struct  {
	int m_nChannel;
	int m_nChannelFlags;
	int m_nRate;
	int m_nAntenna;
	int m_nRadiotapFlags;
} __attribute__((packed)) PENUMBRA_RADIOTAP_DATA;

/*
 * Radiotap parser
 *
 * Copyright 2007		Andy Green <andy@warmcat.com>
 */

/**
 * ieee80211_radiotap_iterator_init - radiotap parser iterator initialization
 * @iterator: radiotap_iterator to initialize
 * @radiotap_header: radiotap header to parse
 * @max_length: total length we can parse into (eg, whole packet length)
 *
 * Returns: 0 or a negative error code if there is a problem.
 *
 * This function initializes an opaque iterator struct which can then
 * be passed to ieee80211_radiotap_iterator_next() to visit every radiotap
 * argument which is present in the header.  It knows about extended
 * present headers and handles them.
 *
 * How to use:
 * call __ieee80211_radiotap_iterator_init() to init a semi-opaque iterator
 * struct ieee80211_radiotap_iterator (no need to init the struct beforehand)
 * checking for a good 0 return code.  Then loop calling
 * __ieee80211_radiotap_iterator_next()... it returns either 0,
 * -ENOENT if there are no more args to parse, or -EINVAL if there is a problem.
 * The iterator's @this_arg member points to the start of the argument
 * associated with the current argument index that is present, which can be
 * found in the iterator's @this_arg_index member.  This arg index corresponds
 * to the IEEE80211_RADIOTAP_... defines.
 *
 * Radiotap header length:
 * You can find the CPU-endian total radiotap header length in
 * iterator->max_length after executing ieee80211_radiotap_iterator_init()
 * successfully.
 *
 * Example code:
 * See Documentation/networking/radiotap-headers.txt
 */

int ieee80211_radiotap_iterator_init(
    struct ieee80211_radiotap_iterator *iterator,
    struct ieee80211_radiotap_header *radiotap_header,
    int max_length)
{
	/* Linux only supports version 0 radiotap format */
	if (radiotap_header->it_version)
		return -EINVAL;

	/* sanity check for allowed length and radiotap length field */
	if (max_length < le16_to_cpu(radiotap_header->it_len))
		return -EINVAL;

	iterator->rtheader = radiotap_header;
	iterator->max_length = le16_to_cpu(radiotap_header->it_len);
	iterator->arg_index = 0;
	iterator->bitmap_shifter = le32_to_cpu(radiotap_header->it_present);
	iterator->arg = (u8 *)radiotap_header + sizeof(*radiotap_header);
	iterator->this_arg = 0;

	/* find payload start allowing for extended bitmap(s) */

	if (unlikely(iterator->bitmap_shifter & (1<<IEEE80211_RADIOTAP_EXT))) {
		while (le32_to_cpu(*((u32 *)iterator->arg)) &
				   (1<<IEEE80211_RADIOTAP_EXT)) {
			iterator->arg += sizeof(u32);

			/*
			 * check for insanity where the present bitmaps
			 * keep claiming to extend up to or even beyond the
			 * stated radiotap header length
			 */

			if (((ulong)iterator->arg -
			     (ulong)iterator->rtheader) > iterator->max_length)
				return -EINVAL;
		}

		iterator->arg += sizeof(u32);

		/*
		 * no need to check again for blowing past stated radiotap
		 * header length, because ieee80211_radiotap_iterator_next
		 * checks it before it is dereferenced
		 */
	}

	/* we are all initialized happily */

	return 0;
}


/**
 * ieee80211_radiotap_iterator_next - return next radiotap parser iterator arg
 * @iterator: radiotap_iterator to move to next arg (if any)
 *
 * Returns: 0 if there is an argument to handle,
 * -ENOENT if there are no more args or -EINVAL
 * if there is something else wrong.
 *
 * This function provides the next radiotap arg index (IEEE80211_RADIOTAP_*)
 * in @this_arg_index and sets @this_arg to point to the
 * payload for the field.  It takes care of alignment handling and extended
 * present fields.  @this_arg can be changed by the caller (eg,
 * incremented to move inside a compound argument like
 * IEEE80211_RADIOTAP_CHANNEL).  The args pointed to are in
 * little-endian format whatever the endianess of your CPU.
 */

int ieee80211_radiotap_iterator_next(
    struct ieee80211_radiotap_iterator *iterator)
{

	/*
	 * small length lookup table for all radiotap types we heard of
	 * starting from b0 in the bitmap, so we can walk the payload
	 * area of the radiotap header
	 *
	 * There is a requirement to pad args, so that args
	 * of a given length must begin at a boundary of that length
	 * -- but note that compound args are allowed (eg, 2 x u16
	 * for IEEE80211_RADIOTAP_CHANNEL) so total arg length is not
	 * a reliable indicator of alignment requirement.
	 *
	 * upper nybble: content alignment for arg
	 * lower nybble: content length for arg
	 */

	static const u8 rt_sizes[] = {
		[IEEE80211_RADIOTAP_TSFT] = 0x88,
		[IEEE80211_RADIOTAP_FLAGS] = 0x11,
		[IEEE80211_RADIOTAP_RATE] = 0x11,
		[IEEE80211_RADIOTAP_CHANNEL] = 0x24,
		[IEEE80211_RADIOTAP_FHSS] = 0x22,
		[IEEE80211_RADIOTAP_DBM_ANTSIGNAL] = 0x11,
		[IEEE80211_RADIOTAP_DBM_ANTNOISE] = 0x11,
		[IEEE80211_RADIOTAP_LOCK_QUALITY] = 0x22,
		[IEEE80211_RADIOTAP_TX_ATTENUATION] = 0x22,
		[IEEE80211_RADIOTAP_DB_TX_ATTENUATION] = 0x22,
		[IEEE80211_RADIOTAP_DBM_TX_POWER] = 0x11,
		[IEEE80211_RADIOTAP_ANTENNA] = 0x11,
		[IEEE80211_RADIOTAP_DB_ANTSIGNAL] = 0x11,
		[IEEE80211_RADIOTAP_DB_ANTNOISE] = 0x11
		/*
		 * add more here as they are defined in
		 * include/net/ieee80211_radiotap.h
		 */
	};

	/*
	 * for every radiotap entry we can at
	 * least skip (by knowing the length)...
	 */

	while (iterator->arg_index < sizeof(rt_sizes)) {
		int hit = 0;
		int pad;

		if (!(iterator->bitmap_shifter & 1))
			goto next_entry; /* arg not present */

		/*
		 * arg is present, account for alignment padding
		 *  8-bit args can be at any alignment
		 * 16-bit args must start on 16-bit boundary
		 * 32-bit args must start on 32-bit boundary
		 * 64-bit args must start on 64-bit boundary
		 *
		 * note that total arg size can differ from alignment of
		 * elements inside arg, so we use upper nybble of length
		 * table to base alignment on
		 *
		 * also note: these alignments are ** relative to the
		 * start of the radiotap header **.  There is no guarantee
		 * that the radiotap header itself is aligned on any
		 * kind of boundary.
		 */

		pad = (((ulong)iterator->arg) -
			((ulong)iterator->rtheader)) &
			((rt_sizes[iterator->arg_index] >> 4) - 1);

		if (pad)
			iterator->arg +=
				(rt_sizes[iterator->arg_index] >> 4) - pad;

		/*
		 * this is what we will return to user, but we need to
		 * move on first so next call has something fresh to test
		 */
		iterator->this_arg_index = iterator->arg_index;
		iterator->this_arg = iterator->arg;
		hit = 1;

		/* internally move on the size of this arg */
		iterator->arg += rt_sizes[iterator->arg_index] & 0x0f;

		/*
		 * check for insanity where we are given a bitmap that
		 * claims to have more arg content than the length of the
		 * radiotap section.  We will normally end up equalling this
		 * max_length on the last arg, never exceeding it.
		 */

		if (((ulong)iterator->arg - (ulong)iterator->rtheader) >
		    iterator->max_length)
			return -EINVAL;

	next_entry:
		iterator->arg_index++;
		if (unlikely((iterator->arg_index & 31) == 0)) {
			/* completed current u32 bitmap */
			if (iterator->bitmap_shifter & 1) {
				/* b31 was set, there is more */
				/* move to next u32 bitmap */
				iterator->bitmap_shifter =
				    le32_to_cpu(*iterator->next_bitmap);
				iterator->next_bitmap++;
			} else {
				/* no more bitmaps: end */
				iterator->arg_index = sizeof(rt_sizes);
			}
		} else { /* just try the next bit */
			iterator->bitmap_shifter >>= 1;
		}

		/* if we found a valid arg earlier, return it now */
		if (hit)
			return 0;
	}

	/* we don't know how to handle any more args, we're done */
	return -ENOENT;
}

void set_port_no(uint8_t *pu, uint8_t port) {
	//dirty hack: the last byte of the mac address is the port number. this makes it easy to filter out specific ports via wireshark
	pu[sizeof(u8aRadiotapHeader) + SRC_MAC_LASTBYTE] = port;
	pu[sizeof(u8aRadiotapHeader) + DST_MAC_LASTBYTE] = port;
}


int packet_header_init(uint8_t *packet_header) {
			u8 *pu8 = packet_header;
			memcpy(packet_header, u8aRadiotapHeader, sizeof(u8aRadiotapHeader));
			pu8 += sizeof(u8aRadiotapHeader);
			memcpy(pu8, u8aIeeeHeader, sizeof (u8aIeeeHeader));
			pu8 += sizeof (u8aIeeeHeader);
					
			//determine the length of the header
			return pu8 - packet_header;
}

void pcap_interface_open_common(pcap_t *ppcap_pointer, int initiate_selectable_fd, int port)
{
	struct bpf_program bpfprogram;
	char szProgram[512];
	
	if(pcap_setnonblock(ppcap_pointer, 1, szErrbuf) < 0) {
		fprintf(stderr, "Error setting %s to nonblocking mode: %s\n", INTERFACE_NAME, szErrbuf);
	}

	int nLinkEncap = pcap_datalink(ppcap_pointer);

	
	switch (nLinkEncap) {

		case DLT_PRISM_HEADER:
			//fprintf(stderr, "DLT_PRISM_HEADER Encap\n");
			n80211HeaderLength = 0x20; // ieee80211 comes after this
			sprintf(szProgram, "radio[0x4a:4]==0x13223344 && radio[0x4e:2] == 0x55%.2x", port);
			break;

		case DLT_IEEE802_11_RADIO:
			//fprintf(stderr, "DLT_IEEE802_11_RADIO Encap\n");
			n80211HeaderLength = 0x18; // ieee80211 comes after this
			sprintf(szProgram, "ether[0x0a:4]==0x13223344 && ether[0x0e:2] == 0x55%.2x", port);
			break;

		default:
			//fprintf(stderr, "!!! unknown encapsulation on %s !\n", INTERFACE_NAME);
			exit(1);
	}

	
	if (pcap_compile(ppcap_pointer, &bpfprogram, szProgram, 1, 0) == -1) {
		puts(szProgram);
		puts(pcap_geterr(ppcap));
		exit(1);
	} else {
		if (pcap_setfilter(ppcap_pointer, &bpfprogram) == -1) {
			fprintf(stderr, "%s\n", szProgram);
			fprintf(stderr, "%s\n", pcap_geterr(ppcap));
		} else {
		}
		pcap_freecode(&bpfprogram);
	}
	
	pcap_setnonblock(ppcap_pointer, 1, szErrbuf);
	
	if(selectable_fd==1)
	{
		selectable_fd = pcap_get_selectable_fd(ppcap_pointer);
	}
}

void pcap_interface_open()
{
	// open the interface in pcap
	szErrbuf[0] = '\0';
	ppcap = pcap_open_live(INTERFACE_NAME, BUFSIZ, 0, 1000, szErrbuf);
	if (ppcap == NULL) {
		printf("Unable to open interface %s in pcap: %s\n",
		    INTERFACE_NAME, szErrbuf);
		exit(1);
	}
	
	pcap_interface_open_common(ppcap,0,0);
}

void pb_transmit_packet(pcap_t *ppcap, uint32_t seq_nr, uint8_t *packet_transmit_buffer, int packet_header_len, uint8_t *packet_data, int packet_length, uint32_t nalu_length, uint8_t fec_index) {
	int i=0;
	
	//printf("SEQ: %d\tLAST: %d\n", seq_nr, packet_length);

	discard_packet=1;
	
	size_t offset = packet_header_len;

    //add header
    wifi_packet_header_t *wph = (wifi_packet_header_t*)(packet_transmit_buffer + offset);
    wph->sequence_number = seq_nr;
    wph->nalu_index=pcnt_frame_count;
    wph->nalu_length=nalu_length;
    wph->fec_index=fec_index;
    
		offset += sizeof(wifi_packet_header_t);

    //copy data
    memcpy(packet_transmit_buffer + offset, packet_data, packet_length);
    offset += packet_length;
	
	if(pcap_inject(ppcap, packet_transmit_buffer, offset)==-1)
	{
		usleep(10000);
		printf("pcap close\n");
		//pcap_close(ppcap);
		printf("pcap closed\n");
		
		
		
		//pcap_interface_open();
		i++;
		
		printf("trouble injecting package, waiting a bit %d %d %d\n", offset, packet_length,i);
		
		if(i==5)
		{
			pcap_perror(ppcap, "Trouble injecting packet");
			pcap_close(ppcap);
			exit(1);
		}
		
		//usleep(100000);
	}
	
	discard_packet=0;
}

void init_tx(void) {
	init_done = 1;
	
	telemetry_packet[0]='$';
	telemetry_packet[1]='T';
	
	if (pthread_mutex_init(&lock, NULL) != 0)
    {
        printf("\n mutex init failed\n");
    }
	
    //initialize forward error correction
	fec_init();
    
	printf("Initializing wifibroadcast txhook\n");
	packet_header_length_video = packet_header_init(packet_transmit_buffer_video);
	set_port_no(packet_transmit_buffer_video, 0);
	
	packet_header_length_telemetry = packet_header_init(packet_transmit_buffer_telemetry);
	set_port_no(packet_transmit_buffer_telemetry, 1);
	
	pcap_interface_open();
	
	// open the rc commands interface in pcap
	/*szErrbuf[0] = '\0';
	ppcap_rc_commands = pcap_open_live(INTERFACE_NAME, 2048, 0, -1, szErrbuf);
	if (ppcap_rc_commands == NULL) {
		printf("Unable to open interface %s in pcap: %s\n",
		    INTERFACE_NAME, szErrbuf);
		exit(1);
	}
	
	pcap_interface_open_common(ppcap_rc_commands,1,2);*/
	
	
	//**************** configure serial port ********************************/
	uart0_filestream = open(SERIAL_DEVICE, O_RDWR | O_NOCTTY | O_NDELAY);	
	
	if (uart0_filestream == -1)
	{
		//ERROR - CAN'T OPEN SERIAL PORT
		fprintf(stderr, "Error - Unable to open UART for telemetry.  Ensure it is not in use by another application\n");
	}
	
	//CONFIGURE THE UART
	//The flags (defined in /usr/include/termios.h - see http://pubs.opengroup.org/onlinepubs/007908799/xsh/termios.h.html):
	//	Baud rate:- B1200, B2400, B4800, B9600, B19200, B38400, B57600, B115200, B230400, B460800, B500000, B576000, B921600, B1000000, B1152000, B1500000, B2000000, B2500000, B3000000, B3500000, B4000000
	//	CSIZE:- CS5, CS6, CS7, CS8
	//	CLOCAL - Ignore modem status lines
	//	CREAD - Enable receiver
	//	IGNPAR = Ignore characters with parity errors
	//	ICRNL - Map CR to NL on input (Use for ASCII comms where you want to auto correct end of line characters - don't use for bianry comms!)
	//	PARENB - Parity enable
	//	PARODD - Odd parity (else even)
	struct termios options;
	tcgetattr(uart0_filestream, &options);
	
	cfmakeraw(&options);
	
	//set baudrate
	cfsetispeed(&options, BAUD_RATE);
	cfsetospeed(&options, BAUD_RATE);
	
	//enable receiving
	options.c_cflag |=CREAD;

	//minimum number of chars to read
	options.c_cc[VMIN]=0;
	//timeout reading
	options.c_cc[VTIME]=0;
	//no flowcontrol
	//options.c_cflag &= ~CNEW_RTSCTS;
	
	//write options
	tcsetattr(uart0_filestream, TCSANOW, &options);
	
	start_time = time(NULL);
	
	/*selectable_fd = pcap_get_selectable_fd(ppcap_rc_commands);
	
	int err;
	
	err = pthread_create(&tid, NULL, &process_stick, NULL);
	
	if (err != 0)
            printf("\ncan't create thread :[%s]", strerror(err));*/
}


void process_nalu(void *ptr, size_t len) {
	
	//printf("process nalu frame len %d	%d	%d\n", len, pcnt_video, pcnt_frame_count);
	//printf(".");
	/*if(pcnt_frame_count%100==0)
	{
		printf("100 frames processed\n");
		printf("\n");
	}*/
	
	int data_nbr=(uint8_t)(len/MAX_PACKET_LENGTH);
	int fec_nbr=data_nbr/2+data_nbr%2;
	int i;
	
	uint8_t *data_blocks[data_nbr];
	uint8_t fec_pool[fec_nbr][MAX_PACKET_LENGTH];
	uint8_t *fec_blocks[fec_nbr];
	
	unsigned int data_index=0;
	unsigned int fec_index=0;
	uint32_t len_save=len;
	
	for(i=0;i<data_nbr;i++)
	{
		if(data_index<data_nbr)
		{
			data_blocks[data_index]=ptr;
			data_index++;
		}
		
		if(fec_index<fec_nbr)
		{
			fec_blocks[fec_index]=fec_pool[fec_index];
			fec_index++;
		}
		
		ptr+=MAX_PACKET_LENGTH;
		len-=MAX_PACKET_LENGTH;
	}
	
	uint8_t overall_index;
	
	//send whole block
	if(data_nbr>0)
	{
		fec_encode(MAX_PACKET_LENGTH, data_blocks, data_nbr, fec_blocks, fec_nbr);
		data_index=0;
		fec_index=0;
		overall_index=0;
		
		for(i=0;i<data_nbr;i++)
		{
			if(data_index<data_nbr)
			{
				//if(DEBUG==1)
					//fprintf(stderr, "data %d/%d:%d,%d==>%d\n", data_index, data_nbr, pcnt_video, MAX_PACKET_LENGTH, pcnt_video);
				//write(STDOUT_FILENO, data_blocks[data_index], MAX_PACKET_LENGTH);
				pb_transmit_packet(ppcap, pcnt_video, packet_transmit_buffer_video, packet_header_length_video, data_blocks[data_index], MAX_PACKET_LENGTH, len_save, overall_index);
				
				pcnt_video++;
				data_index++;
				overall_index++;
				usleep(20);
			}
			
			if(fec_index<fec_nbr)
			{
				//if(DEBUG==1)
					//printf(stderr, "fec %d/%d:%d,%d==>%d\n", fec_index,fec_nbr, pcnt_video, MAX_PACKET_LENGTH, pcnt_video);
				
				pb_transmit_packet(ppcap, pcnt_video, packet_transmit_buffer_video, packet_header_length_video, fec_blocks[fec_index], MAX_PACKET_LENGTH, len_save, overall_index);
				
				pcnt_video++;
				fec_index++;
				overall_index++;
				usleep(20);
			}
		}
	}
	
	//write(STDOUT_FILENO, ptr, len);
	
	//send last block twice
	for(i=0;i<2;i++)
	{
		//if(DEBUG==1)
			//printf(stderr, "last %d,%d\n", pcnt_video, len);
		
		pb_transmit_packet(ppcap, pcnt_video, packet_transmit_buffer_video, packet_header_length_video, ptr, len, len_save, overall_index);
		
		pcnt_video++;
		usleep(20);
	}
	
	pcnt_frame_count++;
}


int read_telemetry_packet(int serial_file)
{
	uint8_t char_read;
	size_t read_size;
	
	while((read_size=read(serial_file, &char_read, 1))==1)
	{
		//printf("char read:%x\n", char_read);
		
		switch(char_read)
		{
		case '$':
			telemetry_header_index=1;
			break;
			
		case 'T':
			if(telemetry_header_index==1)
			{
				//telemetry_packet_index--;
				return 1;
			}
			else
			{
				telemetry_packet[telemetry_packet_index]='T';
				telemetry_packet_index++;
				telemetry_header_index=0;
			}
			break;
			
		default:
			if(telemetry_header_index==1)
			{
				telemetry_packet[telemetry_packet_index]='$';
				telemetry_packet_index++;
			}
			
			telemetry_header_index=0;
			telemetry_packet[telemetry_packet_index]=char_read;
			telemetry_packet_index++;
		}
	}
	
//	printf("read_size %d\n", read_size);
	
	return 0;
}

size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream) {
	
	if(fwrite_lock==1)
	{
		fprintf(stderr, "fwrite lock nalu frame discarded\n");
		return nmemb;
	}
	
	fwrite_lock=1;
	
	if(!init_done)
	{
		init_tx();
	}
	
	if(discard_packet==1)
	{
		fprintf(stderr,"discarded one frame of size %d\n", nmemb);
		return 0;
	}
	
	//write(STDOUT_FILENO, ptr, nmemb);
	
	/*if(DEBUG==1)
		fprintf(stderr, "\nnalu %d ==================\n", (int)nmemb);
	else
		fprintf(stderr, "\n");
	*/
	process_nalu((void *)ptr, nmemb);
	
	
	
	//check if telemetry packet is not to be sent
	if(read_telemetry_packet(uart0_filestream)==1)
	{
		/*printf("transmit telemetry\n");
		int i;
		
		for(i=0;i<telemetry_packet_index;i++)
			printf("%x ", telemetry_packet[i]);
		printf("\n");
		*/
		
		pthread_mutex_lock(&lock);
		
		pb_transmit_packet(ppcap, pcnt_telemetry, packet_transmit_buffer_telemetry, packet_header_length_telemetry, telemetry_packet, telemetry_packet_index,0,0);
		usleep(20);
		pcnt_telemetry++;
		//pb_transmit_packet(ppcap, pcnt_telemetry, packet_transmit_buffer_telemetry, packet_header_length_telemetry, telemetry_packet, telemetry_packet_index);
		//pcnt_telemetry++;
		
		
		pthread_mutex_unlock(&lock);
		
		telemetry_packet_index=2;
		telemetry_header_index=0;
	}
	
	fwrite_lock=0;
	return nmemb;
}


